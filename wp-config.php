<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cochranlaw' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*OQ|tY1T&pHZ] ~bXO@(nkUB=#Cn*=_6}.iK~Y;Qq0ADZpI! PGqw_^S5no{O7YA' );
define( 'SECURE_AUTH_KEY',  '_E6Ilzg<yiIBM7!c?[2<C$`:%WgY35!Fq0ci{[6zTVT-}bM;%Sf`zX8w<8}v0Y~|' );
define( 'LOGGED_IN_KEY',    '`|rnK<1u;M`>%?]xFO!^Sf1Tv y!iJ*%QMw=,0^_tP3?-vBvb0y<Z-Q!8!L.Zj#.' );
define( 'NONCE_KEY',        '7s!X5}VZ:Q/u_6;9*>YcBv6=sfYbx$+^66@)(S>3d3kN TINGG:szU{i=fhov]o%' );
define( 'AUTH_SALT',        '/n+a:+L4qo@8gW(S77K`d9<x}S+llqJaQ05OOTs9%oBM}x&7}F9_E1~ySdMym*jC' );
define( 'SECURE_AUTH_SALT', '_W4jMO3WcAErt,-1HV.~WWfXeN4Q!_iN04?U$&>h}x[mEt,lKHH1>RvP(UK`lik=' );
define( 'LOGGED_IN_SALT',   'gsR].?V51pa8j68%)I&B`K#7}v8ur6dkQE.kg| y_]!beV 97=MBd ]?#+^uambw' );
define( 'NONCE_SALT',       'pQ(Ry7i>-/=h@p[{$ii!}K_:RC0RKl+q{;5~rL;|{ie1=>dYUy5*w8sBt8_L;suf' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
