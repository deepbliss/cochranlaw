<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/mediaquery.css"> 
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mmenu.all.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/easy-responsive-tabs.css">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Great+Vibes&display=swap&subset=latin-ext" rel="stylesheet">  
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
		<div class="bg_img" style="background: url(<?php the_field('home_banner');?>)no-repeat;background-size: cover;">
			<header>
				<div class="container">
					<div class="logo">
						<a href="<?php echo site_url();?>"><img src="<?php the_field('header_logo','option');?>"></a>
					</div>
					<div class="header_menu">
    					<?php wp_nav_menu( array('menu'=> 'Header Menu') );?>	
					</div>
					<div class="header_call">
						<div class="toll_free"><?php the_field('header_call_text','option');?></div>
						<div class="phone_no"><?php the_field('header_number','option');?></div>	
					</div>
				</div>			
			</header>
			<div class="container">
				<div class="bg_section">
					<div class="bg_content">
						<?php the_field('home_banner_text');?>
					</div>
					<div class="bg_form">
						<?php echo do_shortcode('[contact-form-7 id="40" title="Contact form 1"]');?>
					</div>
				</div>
			</div>
		</div>	

