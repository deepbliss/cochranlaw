<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="home_content">
	<div class="container">
		<h2><?php the_field('home_content_title');?></h2>
		<h3><?php the_field('home_content_sub_title');?></h3>
	</div>
	<div class="home_content_section">
		<div class="container">
			<div class="home_content_bar">
				<?php the_field('home_content');?>
			</div>
			<div class="home_content_img">
				<figure>
					<span>
						<img src="<?php the_field('home_content_image');?>">
					</span>
				</figure>	
			</div>
		</div>
	</div>	
</div>
<div class="practice_area">
	<div class="container">
		<div class="practice_title">
			<h2><?php the_field('practice_area_title');?></h2>
		</div>	
	</div>
	<?php if( have_rows('home_practice_area') ):?>
	<ul>
		<?php while ( have_rows('home_practice_area') ) : the_row();?>
		<li>
			<figure>
				<a href="">
					<img src="<?php the_sub_field('practice_area_image');?>">
					<!-- <div class="overlay"></div> -->
				</a>
			</figure>
			<h4><a href="#"><?php the_sub_field('practice_area_title');?></a></h4>
		</li>
		<?php endwhile;?>
	</ul>
	<?php endif;?>
</div>
<div class="track_record">
	<div class="track_record_title">
		<h2><?php the_field('result_drive_title');?></h2>
	</div>
	<div class="outer">
		<div class="thumbs_bg">
			<div class="container">
				<?php if( have_rows('home_testimonial') ):?>
				<div id="thumbs" class="owl-carousel">
					
					<?php while ( have_rows('home_testimonial') ) : the_row();?>
					<div class="item">
		    			<h1><?php the_sub_field('testimonial_number');?></h1>
		  			</div>
		  			<?php endwhile;?>
				</div>
				<?php endif;?>
			</div>
		</div>			
		<div id="big" class="owl-carousel">
			<?php if( have_rows('home_testimonial') ):
				while ( have_rows('home_testimonial') ) : the_row();?>
				<div class="item">
				    <div class="col col-image">
				    	<div class="col-img" style="background: url('<?php the_sub_field('testimonial_image');?>');"></div>
				    </div>
				    <div class="col col-text">
				      <div class="Aligner-item">
				        <?php the_sub_field('testimonial_content');?>
				      </div>
				    </div>
  				</div>
  				<?php endwhile; endif;?>
		</div>	
	</div>	
</div>
<div class="call_to_free">
	<div class="container">
		<div class="call_free_space">
			<div class="lft_sec">
				<?php the_field('call_to_free_number_text');?>	
			</div>
			<div class="rgt_sec">
				
				<div class="call">
					<a href="tel:8666424529"><?php the_field('call_to_free_number');?></a>	
				</div>

			</div>
		</div>		
	</div>
</div>
<div class="qualified">
	<div class="container">
		<div class="qualified_content">
			<?php the_field('qualified_content');?>
		</div>
	</div>
</div>
<div class="client_review">
	<div class="container">
		<div class="client_review_box">
			<div class="client_review_title">
				<h2>CLIENT <span>REVIEWS</span></h2>
			</div>	
			<?php if( have_rows('client_review') ):?>
			<ul id="client_reviews" class="owl-carousel">
				<?php while ( have_rows('client_review') ) : the_row();?>
				<li>
					<h4><?php the_sub_field('review_title');?></h4>	
					<?php the_sub_field('review_content');?>
					<span>-<?php the_sub_field('review_person');?></span>
				</li>
				<?php endwhile;?>
			</ul>
			<?php endif;?>
			<a href="#" class="review">LEAVE A REVIEW</a>
		</div>	
	</div>
</div>
<div class="toll_free">
	<div class="container">
		<div class="toll_free_sec">
			<div class="lft_sec">
				<?php the_field('call_toll_free');?>
			</div>
			<div class="rgt_sec">
				<div class="call">
					<a href="tel:8666424529"><?php the_field('call_to_free_number');?></a>	
				</div>
			</div>	
		</div>
	</div>	
</div>
<div class="attorneys">
	<?php the_field('attorneys_title');?>
	<?php if( have_rows('attroneys_team') ):?>
	<ul class="owl-carousel attorneys_bar">
		<?php while ( have_rows('attroneys_team') ) : the_row();?>
		<li>
			<div class="team_img">
				<figure>
					<span>
						<img src="<?php the_sub_field('team_image');?>">
					</span>	
				</figure>	
			</div>
			<h4><?php the_sub_field('team_title');?></h4>
			<h5><?php the_sub_field('team_position');?></h5>
		</li>
		<?php endwhile;?>
	</ul>
	<?php endif;?>
</div>
<div class="news_section">
	<div class="container">
		<div class="news_title">
			<h2>RECENT <span>NEWS</span></h2>
		</div>	
		<div class="news">
			<ul>
				<?php 
					   // the query
					   $the_query = new WP_Query( array(
					     'posts_per_page' => 3,
					     'order' => 'ASC',
					   )); 
					?>
					<?php if ( $the_query->have_posts() ) : ?>
					  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<li>
					  	<div class="featured_img">
					  		<figure>
					  			<span><?php the_post_thumbnail();?></span>
					  		</figure>
					  	</div>
					    <div class="news_desc">
					    	<h2><?php the_title(); ?></h2>
					    	<a href="<?php the_permalink();?>" class="read_more">READ MORE</a>
					    </div>
					  
					  <?php wp_reset_postdata(); ?>

					
					
				</li>
				<?php endwhile; ?>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</div>
<div class="free_consultation_sec">
	<div class="container">
		<div class="free_consultation">
			<div class="lft_bar">
				<h2>FREE CONSULTATION <span>NO FEES UNTIL WE WIN</span></h2>
				<div class="description">There is no obligation for a case evaluation & no fee is charged unless a recovery is made.</div>
			</div>	
			<div class="rgt_bar">
				<div class="bg_section">
					<div class="bg_form">
						<?php echo do_shortcode('[contact-form-7 id="40" title="Contact form 1"]');?>
					</div>
				</div>
			</div>
			<small>Your privacy is important to us. Cochran, Kroll & Associates, P.C. does not share, sell, rent, or trade personally identifiable or confidential information with third parties for any purpose.</small>
		</div>
	</div>
</div>
<?php
get_footer();
